// Activity Template:
// (Copy this part on your template)


/*
	1.) Create a function that returns a passed string with letters in alphabetical order.
		Example string: 'mastermind'
		Expected output: 'adeimmnrst'
	
*/

// Code here:

function arrangeAlpha(str){
    return str.split("").sort().join("");
}

console.log(arrangeAlpha('mastermind'));

/*
	2.) Write a simple JavaScript program to join all elements of the following array into a string.

	Sample array : myColor = ["Red", "Green", "White", "Black"];
		Expected output: 
			Red,Green,White,Black
			Red,Green,White,Black
			Red+Green+White+Black

*/

// Code here:

function joinArr(){
    let myColor = ["Red", "Green", "White", "Black"];
    console.log(myColor.join(","));
    console.log(myColor.join(","));
    console.log(myColor.join("+"));
}

joinArr();

/*
	3.) Write a function named birthdayGift that pass a gift as an argument.
		- if the gift is a stuffed toy, return a message that says: "Thank you for the stuffed toy, Michael!"
		- if the gift is a doll, return a message that says: "Thank you for the doll, Sarah!"
		- if the gift is a cake, return a message that says: "Thank you for the cake, Donna!"
		- if other gifts return a message that says: "Thank you for the (gift), Dad!"
		- create a global variable named myGift and invoke the function in the variable.
		- log the global var in the console

*/

// Code here:

function birthdayGift(gift){

    let msg;

    switch (gift) {
        case 'stuffed toy':
            msg = 'Thank you for the stuffed toy, Michael!';
            break;

        case 'doll':
            msg = 'Thank you for the doll, Sarah!';
            break;

        case 'cake':
            msg = 'Thank you for the cake, Donna!';
            break;
    
        default:
            msg = 'Thank you for the ' + gift + ', Dad!';
            break;

    }

    return msg;
}


let myGift = birthdayGift('cake');
console.log(myGift)

/*
	4.) Write a function that accepts a string as a parameter and counts the number of vowels within the string.

		Example string: 'The quick brown fox'
		Expected Output: 5

*/

// Code here:

function countVowels(str){

    let count = 0;
    let vowels = ['a','e','i','o','u'];

    for(let i = 0; i < str.length; i++){

        let char = str[i];
        let isVowel = vowels.includes(char);

        if(isVowel){
            count++;
        }

    }

    return count;

}

console.log(countVowels('The quick brown fox'));